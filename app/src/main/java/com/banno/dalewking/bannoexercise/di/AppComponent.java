package com.banno.dalewking.bannoexercise.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ViewModelModule.class} )
public interface AppComponent extends AppInjectors
{
}

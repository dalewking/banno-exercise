package com.banno.dalewking.bannoexercise.viewmodels;

import io.reactivex.Observable;

public interface ImageMediaScreenViewModel extends BaseViewModel
{
    Observable<Integer> getClickCount();

    void click();
}

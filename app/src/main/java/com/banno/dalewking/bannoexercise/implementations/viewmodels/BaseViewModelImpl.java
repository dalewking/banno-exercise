package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.viewmodels.BaseViewModel;

import javax.inject.Inject;

public class BaseViewModelImpl implements BaseViewModel
{
    @Inject
    InstagramAccount instagramAccount;

    @Override
    public boolean isUserLoggedIn()
    {
        return instagramAccount.isLoggedIn();
    }
}

package com.banno.dalewking.bannoexercise.views;

import android.content.Intent;
import android.os.Bundle;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.viewmodels.LoginScreenViewModel;

import javax.inject.Inject;

import butterknife.OnClick;

public class LoginActivity extends BaseActivity
{
    @Inject
    LoginScreenViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);

        new LoginActivity_ViewBinding(this);
    }

    @OnClick(R.id.login)
    void login()
    {
        startActivity(new Intent(this, SignInActivity.class));
    }
}

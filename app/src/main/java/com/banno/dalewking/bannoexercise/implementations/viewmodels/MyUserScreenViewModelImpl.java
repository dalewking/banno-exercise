package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.viewmodels.MyUserScreenViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class MyUserScreenViewModelImpl extends BaseViewModelImpl implements MyUserScreenViewModel
{
    @Inject
    public MyUserScreenViewModelImpl()
    {
    }

    @Override
    public Single<InstagramAccount.UserInfo> getUserInfo()
    {
        return instagramAccount.getUserInfo();
    }
}

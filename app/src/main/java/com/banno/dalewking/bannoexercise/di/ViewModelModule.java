package com.banno.dalewking.bannoexercise.di;

import com.banno.dalewking.bannoexercise.implementations.viewmodels.ImageMediaScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.implementations.viewmodels.LoginScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.implementations.viewmodels.MyLikesScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.implementations.viewmodels.MyMediaScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.implementations.viewmodels.MyUserScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.implementations.viewmodels.SignInScreenViewModelImpl;
import com.banno.dalewking.bannoexercise.viewmodels.ImageMediaScreenViewModel;
import com.banno.dalewking.bannoexercise.viewmodels.LoginScreenViewModel;
import com.banno.dalewking.bannoexercise.viewmodels.MyLikesScreenViewModel;
import com.banno.dalewking.bannoexercise.viewmodels.MyMediaScreenViewModel;
import com.banno.dalewking.bannoexercise.viewmodels.MyUserScreenViewModel;
import com.banno.dalewking.bannoexercise.viewmodels.SignInScreenViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelModule
{
    @Binds @Singleton
    abstract LoginScreenViewModel bindLoginViewModel(LoginScreenViewModelImpl impl);

    @Binds @Singleton
    abstract SignInScreenViewModel bindSignInViewModel(SignInScreenViewModelImpl impl);

    @Binds @Singleton
    abstract MyUserScreenViewModel bindMainViewModel(MyUserScreenViewModelImpl impl);

    @Binds @Singleton
    abstract MyMediaScreenViewModel bindMyMediaViewModel(MyMediaScreenViewModelImpl impl);

    @Binds @Singleton
    abstract MyLikesScreenViewModel bindMyLikesViewModel(MyLikesScreenViewModelImpl impl);

    @Binds @Singleton
    abstract ImageMediaScreenViewModel bindImageMediaViewModel(ImageMediaScreenViewModelImpl impl);
}

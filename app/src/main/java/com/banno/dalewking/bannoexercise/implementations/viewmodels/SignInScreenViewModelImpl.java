package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.viewmodels.SignInScreenViewModel;

import javax.inject.Inject;


public class SignInScreenViewModelImpl extends BaseViewModelImpl implements SignInScreenViewModel
{
    @Inject
    public SignInScreenViewModelImpl()
    {
    }

    @Override
    public String getRedirectUrl()
    {
        return instagramAccount.getRedirectUrl();
    }

    @Override
    public String getAuthenticationUrl()
    {
        return instagramAccount.getAuthenticationUrl();
    }

    @Override
    public void setToken(String token)
    {
        instagramAccount.setNewToken(token);
    }
}

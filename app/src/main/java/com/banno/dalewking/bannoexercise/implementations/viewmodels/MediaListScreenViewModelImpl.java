package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import android.security.keystore.UserNotAuthenticatedException;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.models.InstagramApi;
import com.banno.dalewking.bannoexercise.viewmodels.MediaListScreenViewModel;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public abstract class MediaListScreenViewModelImpl extends BaseViewModelImpl
{
    private List<InstagramApi.MediaItem> items = new ArrayList<>();
    private BehaviorRelay<List<InstagramApi.MediaItem>> itemRelay
            = BehaviorRelay.createDefault(items);
    private BehaviorRelay<MediaListScreenViewModel.State> state = BehaviorRelay.createDefault(MediaListScreenViewModel.State.MORE_DATA_TO_LOAD);
    private Single<InstagramAccount.MediaListPage> dataSource;

    protected abstract Single<InstagramAccount.MediaListPage> getDataSource();

    @Inject
    void afterInjection()
    {
        dataSource = getDataSource();

        loadMoreItems();
    }

    public Observable<List<InstagramApi.MediaItem>> getItems()
    {
        return itemRelay;
    }

    public Observable<MediaListScreenViewModel.State> getState()
    {
        return state;
    }

    private void update(InstagramAccount.MediaListPage page)
    {
        items.addAll(page.getItems());
        itemRelay.accept(items);

        dataSource = page.getNextPage();

        if(page.hasMorePages())
        {
            state.accept(MediaListScreenViewModel.State.MORE_DATA_TO_LOAD);
        }
        else
        {
            state.accept(MediaListScreenViewModel.State.NO_MORE_DATA);
        }
    }

    private void handleError(Throwable error)
    {
        if(error instanceof UserNotAuthenticatedException)
        {
            state.accept(MediaListScreenViewModel.State.AUTHENTICATION_ERROR);
        }
        else
        {
            state.accept(MediaListScreenViewModel.State.ERROR);
        }
    }

    public void loadMoreItems()
    {
        // Only load if we are in the right state
        switch(state.getValue())
        {
            case MORE_DATA_TO_LOAD:
            case ERROR:
                dataSource
                        .doOnSubscribe(s -> state.accept(MediaListScreenViewModel.State.LOADING))
                        .subscribe(this::update, this::handleError);
                break;

            default:
                break;
        }
    }
}

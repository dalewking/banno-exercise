package com.banno.dalewking.bannoexercise.core;

import android.app.Application;

import com.banno.dalewking.bannoexercise.di.ExerciseInjector;
import com.nobleworks_software.injection.android.InjectionService;

public class ExerciseApplication extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();

        InjectionService.setInjector(new ExerciseInjector());
    }
}

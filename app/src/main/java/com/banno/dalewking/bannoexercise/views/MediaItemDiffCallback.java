package com.banno.dalewking.bannoexercise.views;

import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;

import com.banno.dalewking.bannoexercise.models.InstagramApi;

import java.util.List;

public class MediaItemDiffCallback extends DiffUtil.Callback
{
    private final List<InstagramApi.MediaItem> oldList;
    private final List<InstagramApi.MediaItem> newList;

    public MediaItemDiffCallback(List<InstagramApi.MediaItem> oldList, List<InstagramApi.MediaItem> newList)
    {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize()
    {
        return oldList.size();
    }

    @Override
    public int getNewListSize()
    {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
    {
        return oldList.get(oldItemPosition).id == newList.get(newItemPosition).id;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
    {
        return areItemsTheSame(oldItemPosition, newItemPosition);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition)
    {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
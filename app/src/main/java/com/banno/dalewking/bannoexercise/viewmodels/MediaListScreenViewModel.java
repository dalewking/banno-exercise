package com.banno.dalewking.bannoexercise.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramApi;

import java.util.List;

import io.reactivex.Observable;

public interface MediaListScreenViewModel extends BaseViewModel
{
    Observable<List<InstagramApi.MediaItem>> getItems();

    Observable<State> getState();

    void loadMoreItems();

    enum State
    {
        LOADING, ERROR, AUTHENTICATION_ERROR, MORE_DATA_TO_LOAD, NO_MORE_DATA
    }
}

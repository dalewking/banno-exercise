package com.banno.dalewking.bannoexercise.views;

import android.os.Bundle;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.databinding.MyUserInfoViewLayoutBinding;
import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.viewmodels.MyUserScreenViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class MyUserActivity extends NavigationDrawerActivity<MyUserScreenViewModel>
{
    @BindView(R.id.loadingView)
    LoadingContentErrorView loadingView;

    MyUserInfoViewLayoutBinding binding;

    @Inject
    void setViewModel(MyUserScreenViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.my_user_activity);

        new MyUserActivity_ViewBinding(this);

        binding = MyUserInfoViewLayoutBinding.bind(loadingView.getContentView());

        loadingView.getRetryClicks()
                .subscribe(click -> reload());

        reload();
    }

    private void reload()
    {
        viewModel.getUserInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(s -> loadingView.showLoading())
                .compose(bindToLifecycle())
                .subscribe(this::showUserInfo, this::handleError);
    }

    private void handleError(Throwable t)
    {
        loadingView.showError();

        if(t instanceof InstagramAccount.NotAuthenticatedException)
        {
            checkLoggedIn();
        }
    }

    private void showUserInfo(InstagramAccount.UserInfo userInfo)
    {
        binding.setUser(userInfo);

        loadingView.showContent();
    }
}
package com.banno.dalewking.bannoexercise.di;

public interface Names
{
    String REDIRECT_URL = "RedirectUrl";
    String AUTHENTICATION_URL = "AuthenticationUrl";
    String OAUTH_TOKEN = "UserToken";
}

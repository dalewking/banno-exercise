package com.banno.dalewking.bannoexercise.models;

import java.util.List;

import io.reactivex.Single;

public interface InstagramAccount
{
    class NotAuthenticatedException extends Exception
    {
        public NotAuthenticatedException()
        {
        }

        public NotAuthenticatedException(String message)
        {
            super(message);
        }

        public NotAuthenticatedException(String message, Throwable cause)
        {
            super(message, cause);
        }

        public NotAuthenticatedException(Throwable cause)
        {
            super(cause);
        }
    }

    interface UserInfo
    {
        String getId();
        String getUserName();
        String getFullName();
        String getProfilePicture();
        String getBio();
        String getWebSite();
        int getMediaCount();
        int getFollowsCount();
        int getFollowedByCount();
    }

    interface MediaListPage
    {
        List<InstagramApi.MediaItem> getItems();

        boolean hasMorePages();

        Single<MediaListPage> getNextPage();
    }

    Single<UserInfo> getUserInfo();

    Single<MediaListPage> getMyMedia();

    Single<MediaListPage> getMyLikes();

    String getAuthenticationUrl();

    String getRedirectUrl();

    void setNewToken(String token);

    boolean isLoggedIn();
}

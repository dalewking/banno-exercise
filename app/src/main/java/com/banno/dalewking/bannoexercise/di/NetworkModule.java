package com.banno.dalewking.bannoexercise.di;

import android.app.Application;
import android.net.Uri;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.models.InstagramApi;
import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class NetworkModule
{
    public static final String BASE_URL = "https://api.instagram.com/";

    // 50MB cache
    private static final int DISK_CACHE_SIZE = 50 * 1024 * 1024;
    public static final int TIMEOUT_SECONDS = 20;

    @Provides @Singleton @Named(Names.REDIRECT_URL)
    String provideRedirectUrl(Application application)
    {
        return application.getString(R.string.redirectUri);
    }

    @Provides @Singleton @Named(Names.AUTHENTICATION_URL)
    String provideAuthenticationUri(Application application,
                                    @Named(Names.REDIRECT_URL) String redirectUrl)
    {
        return Uri.parse(BASE_URL + "oauth/authorize/").buildUpon()
                .encodedQuery("scope=basic+public_content+likes")
                .appendQueryParameter("client_id", application.getString(R.string.clientId))
                .appendQueryParameter("redirect_uri", redirectUrl)
                .appendQueryParameter("response_type", "token")
                .build()
                .toString();
    }

    @Provides @Named(Names.OAUTH_TOKEN)
    Preference<String> provideTokenPreference(RxSharedPreferences prefs)
    {
        return prefs.getString(Names.OAUTH_TOKEN, "");
    }

    @Provides
    Retrofit provideRestAdapter(OkHttpClient client)
    {
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    @Provides @Singleton
    OkHttpClient provideOkHttpClient(Application app)
    {
        // Install an HTTP cache in the application cache directory.
        File cacheDir = new File(app.getCacheDir(), "http"); //NON-NLS
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);

        return new OkHttpClient.Builder()
                .cache(cache)
                .connectTimeout(TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .build();
    }

    @Provides @Singleton
    InstagramApi provideInstagramApi(Retrofit retrofit)
    {
        return retrofit.create(InstagramApi.class);
    }
}


package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.viewmodels.MyMediaScreenViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class MyMediaScreenViewModelImpl
        extends MediaListScreenViewModelImpl implements MyMediaScreenViewModel
{
    @Inject
    MyMediaScreenViewModelImpl()
    {
    }

    @Override
    protected Single<InstagramAccount.MediaListPage> getDataSource()
    {
        return instagramAccount.getMyMedia();
    }

}

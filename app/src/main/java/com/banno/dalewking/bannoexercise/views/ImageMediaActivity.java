package com.banno.dalewking.bannoexercise.views;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.viewmodels.ImageMediaScreenViewModel;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ImageMediaActivity extends NavigationDrawerActivity<ImageMediaScreenViewModel>
{
    @BindView(R.id.button)
    Button button;

    @BindView(R.id.textView)
    TextView textView;

    @Inject
    void setViewModel(ImageMediaScreenViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.image_media_activity);

        new ImageMediaActivity_ViewBinding(this);

        RxView.clicks(button)
                .compose(RxLifecycleAndroid.bindView(button))
                .subscribe(view -> viewModel.click());

        viewModel.getClickCount()
                .observeOn(AndroidSchedulers.mainThread())
                .map(count -> getString(R.string.clickMessage, count))
                .compose(RxLifecycleAndroid.bindView(textView))
                .subscribe(RxTextView.text(textView));
    }
}
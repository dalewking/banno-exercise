package com.banno.dalewking.bannoexercise.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface InstagramApi
{
    public class PaginationData
    {
        @SerializedName("next_url")
        public String nextUrl;

        @SerializedName("next_max_id")
        public String nextMaxId;
    }

    class Envelope<T>
    {
        public class Metadata
        {
            public Integer code;

            @SerializedName("error_type")
            public String errorType;

            @SerializedName("error_message")
            public String errorMessage;
        }

        public T data;

        public Metadata meta;

        public PaginationData pagination;
    }

    class UserData
    {
        public class Counts
        {
            public int media;
            public int follows;

            @SerializedName("followed_by")
            public int followedBy;
        }

        public String id;

        @SerializedName("username")
        public String userName;

        @SerializedName("full_name")
        public String fullName;

        @SerializedName("profile_picture")
        public String profilePicture;

        public String bio;

        @SerializedName("website")
        public String webSite;

        public Counts counts;
    }

    public class MediaItem
    {
        public class Count
        {
            public int count;
        }

        public class Caption
        {
            public String id;

            public String text;

            public String createdTime;
//                    "from": {
//                "username": "kevin",
//                        "full_name": "Kevin Systrom",
//                        "type": "user",
//                        "id": "3"
//            }
        }

        public class Images
        {
            public class Info
            {
                public String url;
                public int width;
                public int height;
            }

            @SerializedName("thumbnail")
            public Info thumbNail;

            @SerializedName("low_resolution")
            public Info lowResolution;

            @SerializedName("standard_resolution")
            public Info standardResolution;
        }

        public class User
        {
            public String id;

            @SerializedName("username")
            public String userName;

            @SerializedName("full_name")
            public String fullName;

            @SerializedName("profile_picture")
            public String profilePicture;
        }

        public String id;

        public String link;

        public User from;

        public Caption caption;

        @SerializedName("created_time")
        public String createdTime;

        public String type;

        public List<String> tags;

        @SerializedName("comments")
        public Count commentCount;

        @SerializedName("likes")
        public Count likeCount;

        public Images images;
    }

    @GET("v1/users/self")
    Single<Envelope<UserData>>
    getSelfData(@Query("access_token") String token);

    @GET("v1/users/self/media/recent")
    Single<Envelope<MediaItem[]>>
    getMyMedia(@Query("access_token") String token);

    @GET("v1/users/self/media/recent")
    Single<Envelope<MediaItem[]>>
    getMyMedia(@Query("access_token") String token, @Query("max_id")String maxId);

    @GET("v1/users/self/media/liked")
    Single<Envelope<MediaItem[]>>
    getMyLikes(@Query("access_token") String token);

    @GET("v1/users/self/media/liked")
    Single<Envelope<MediaItem[]>>
    getMyLikes(@Query("access_token") String token, @Query("max_id")String maxId);
}

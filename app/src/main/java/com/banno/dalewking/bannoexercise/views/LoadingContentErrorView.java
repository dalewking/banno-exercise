package com.banno.dalewking.bannoexercise.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.ViewStubCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.banno.dalewking.bannoexercise.R;
import com.jakewharton.rxbinding2.view.RxView;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class LoadingContentErrorView extends FrameLayout
{
    public LoadingContentErrorView(Context context)
    {
        super(context);
        
        initialize(context, null);
    }

    public LoadingContentErrorView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        
        initialize(context, attrs);
    }

    private void initialize(Context context, AttributeSet attrs)
    {
        LayoutInflater.from(context).inflate(R.layout.loading_content_error, this);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.LoadingContentErrorView,
                0, 0);

        try
        {
            int layout = a.getResourceId(R.styleable.LoadingContentErrorView_contentLayout, 0);

            if(layout != 0)
            {
                setContent(layout);
            }
        }
        finally
        {
            a.recycle();
        }

        showLoading();
    }

    public void setContent(@LayoutRes int layout)
    {
        ViewStubCompat stub = (ViewStubCompat)findViewById(R.id.loadingViewContentStub);

        if(stub != null)
        {
            stub.setLayoutResource(layout);
            stub.inflate();
        }
        else
        {
            throw new IllegalStateException("could not find content stub to inflate");
        }
    }

    public View getContentView()
    {
        return findViewById(R.id.content);
    }

    private void setDisplayedChild(int viewToShow)
    {
        for(int i = 0; i < getChildCount(); i++)
        {
            View v = getChildAt(i);

            v.setVisibility(viewToShow == v.getId() ? VISIBLE : GONE);
        }
    }

    public void showLoading()
    {
        setDisplayedChild(R.id.loading);
    }

    public void showError()
    {
        setDisplayedChild(R.id.error);
    }

    public void showContent()
    {
        setDisplayedChild(R.id.content);
    }

    public Observable<Object> getRetryClicks()
    {
        View retryButton = findViewById(R.id.retryButton);

        return RxView.clicks(retryButton)
                .compose(RxLifecycleAndroid.bindView(retryButton))
                .subscribeOn(AndroidSchedulers.mainThread());

    }
}

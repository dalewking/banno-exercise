package com.banno.dalewking.bannoexercise.views;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.viewmodels.SignInScreenViewModel;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;

public class SignInActivity extends BaseActivity
{
    @BindView(R.id.signInWebView)
    WebView webView;

    @Inject
    SignInScreenViewModel viewModel;

    // TODO: Add progress indicator
    // TODO: Error handling

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sign_in_activity);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolBar.setNavigationOnClickListener(v -> onBackPressed());

        new SignInActivity_ViewBinding(this);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
        {
            CookieSyncManager.createInstance(this);
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        webView.getSettings().setJavaScriptEnabled(true);

        webView.setWebViewClient(new WebViewClient()
        {
            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                if(url.startsWith(viewModel.getRedirectUrl()))
                {
                    Matcher matcher = Pattern.compile("access_token=(.+)$").matcher(url);

                    if(matcher.find())
                    {
                        viewModel.setToken(matcher.group(1));

                        startActivity(new Intent(SignInActivity.this, MyUserActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                    }
                    else
                    {
                        new AlertDialog.Builder(SignInActivity.this)
                                .setMessage(R.string.authenticationError)
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.ok, null)
                                .show();

                        finishActivity(RESULT_CANCELED);
                    }

                    return true;
                }
                else
                {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }
        });

        webView.loadUrl(viewModel.getAuthenticationUrl());
    }
}

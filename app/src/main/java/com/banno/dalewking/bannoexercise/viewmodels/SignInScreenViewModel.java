package com.banno.dalewking.bannoexercise.viewmodels;

public interface SignInScreenViewModel extends BaseViewModel
{
    String getRedirectUrl();

    String getAuthenticationUrl();

    void setToken(String token);
}

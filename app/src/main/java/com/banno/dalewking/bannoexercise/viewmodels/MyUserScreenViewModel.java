package com.banno.dalewking.bannoexercise.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;

import io.reactivex.Single;

public interface MyUserScreenViewModel extends BaseViewModel
{
    Single<InstagramAccount.UserInfo> getUserInfo();
}

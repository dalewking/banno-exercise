package com.banno.dalewking.bannoexercise.implementations.models;

import android.text.TextUtils;

import com.annimon.stream.function.Function;
import com.banno.dalewking.bannoexercise.di.Names;
import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.models.InstagramApi;
import com.f2prateek.rx.preferences2.Preference;
import com.google.auto.value.AutoValue;

import java.util.Arrays;
import java.util.NoSuchElementException;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class InstagramAccountImpl implements InstagramAccount
{
    @Inject @Named(Names.OAUTH_TOKEN)
    Preference<String> token;

    @Inject
    InstagramApi api;

    @Inject
    InstagramAccountImpl()
    {
    }

    @Inject @Named(Names.AUTHENTICATION_URL)
    String authenticationUrl;

    @Inject @Named(Names.REDIRECT_URL)
    String redirectUrl;

    @Override
    public String getAuthenticationUrl()
    {
        return authenticationUrl;
    }

    @Override
    public String getRedirectUrl()
    {
        return redirectUrl;
    }

    @Override
    public boolean isLoggedIn()
    {
        String value = token.get();
        return !TextUtils.isEmpty(value) && TextUtils.getTrimmedLength(value) > 0;
    }

    /**
     * Convert null and tokens with only whitespace to empty string.
     * @param token input token
     * @return input or empty string if input contains no non-whitespace characters
     */
    private String sanitizeToken(String token)
    {
        if(TextUtils.isEmpty(token) || TextUtils.getTrimmedLength(token) < 1)
        {
            return "";
        }
        else
        {
            return token;
        }
    }

    @Override
    public void setNewToken(String token)
    {
        this.token.set(sanitizeToken(token));
    }

    @AutoValue
    static abstract class UserInfoImpl implements UserInfo
    {
        static UserInfo create(InstagramApi.UserData data)
        {
            return new AutoValue_InstagramAccountImpl_UserInfoImpl(
                    data.id, data.userName, data.fullName, data.profilePicture, data.bio,
                    data.webSite, data.counts.media, data.counts.follows, data.counts.followedBy
            );
        }
    }

    private <T> void checkErrors(InstagramApi.Envelope<T> envelope)
            throws Exception
    {
        if(envelope.data == null)
        {
            if(envelope.meta.errorType.startsWith("OAuth"))
            {
                setNewToken("");
                throw new NotAuthenticatedException(envelope.meta.errorMessage);
            }
            else
            {
                throw new Error(envelope.meta.errorMessage);
            }
        }
    }

    private Single<String> getToken()
    {
        return token.asObservable()
                .firstOrError()
                .doOnSuccess(token ->
                {
                    if(TextUtils.isEmpty(token))
                    {
                        throw new NotAuthenticatedException("token is not set");
                    }
                });
    }

    @Override
    public Single<UserInfo> getUserInfo()
    {
        return getToken()
                .observeOn(Schedulers.io())
                .flatMap(api::getSelfData)
                .doOnSuccess(this::checkErrors)
                .map(envelope -> envelope.data)
                .map(UserInfoImpl::create);
    }

    @AutoValue
    static abstract class MediaListPageImpl implements MediaListPage
    {
        protected abstract @Nullable String getNextMaxId();

        protected abstract Function<String, Single<MediaListPage>> getNextPageFunction();

        public boolean hasMorePages()
        {
            return getNextMaxId() != null;
        }

        public Single<MediaListPage> getNextPage()
        {
            if(hasMorePages())
            {
                return getNextPageFunction().apply(getNextMaxId());
            }
            else
            {
                return Single.error(new NoSuchElementException("no more pages"));
            }
        }

        public static MediaListPageImpl create(InstagramApi.Envelope<InstagramApi.MediaItem[]> data,
                                        Function<String, Single<MediaListPage>> nextPageFunction)
        {
            return new AutoValue_InstagramAccountImpl_MediaListPageImpl(
                    Arrays.asList(data.data), data.pagination.nextMaxId, nextPageFunction);
        }
    }

    @Override
    public Single<MediaListPage> getMyMedia()
    {
        return getToken()
                .observeOn(Schedulers.io())
                .flatMap(api::getMyMedia)
                .doOnSuccess(this::checkErrors)
                .map(envelope -> MediaListPageImpl.create(envelope, this::getMyMedia));
    }

    private Single<MediaListPage> getMyMedia(String maxId)
    {
        return getToken()
                .observeOn(Schedulers.io())
                .flatMap(api::getMyMedia)
                .doOnSuccess(this::checkErrors)
                .map(envelope -> MediaListPageImpl.create(envelope, this::getMyMedia));
    }

    @Override
    public Single<MediaListPage> getMyLikes()
    {
        return getToken()
                .observeOn(Schedulers.io())
                .flatMap(token -> api.getMyLikes(token))
                .doOnSuccess(this::checkErrors)
                .map(envelope -> MediaListPageImpl.create(envelope, this::getMyLikes));
    }

    private Single<MediaListPage> getMyLikes(String maxId)
    {
        return getToken()
                .observeOn(Schedulers.io())
                .flatMap(api::getMyLikes)
                .doOnSuccess(this::checkErrors)
                .map(envelope -> MediaListPageImpl.create(envelope, this::getMyLikes));
    }
}

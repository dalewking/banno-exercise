package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.viewmodels.ImageMediaScreenViewModel;
import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Inject;

import io.reactivex.Observable;

public class ImageMediaScreenViewModelImpl
        extends BaseViewModelImpl implements ImageMediaScreenViewModel
{
    @Inject
    public ImageMediaScreenViewModelImpl()
    {
    }

    private final BehaviorRelay<Integer> count = BehaviorRelay.createDefault(0);

    @Override
    public Observable<Integer> getClickCount()
    {
        return count;
    }

    @Override
    public synchronized void click()
    {
        count.accept(count.getValue() + 1);
    }
}

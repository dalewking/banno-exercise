package com.banno.dalewking.bannoexercise.views;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.SparseArray;
import android.view.MenuItem;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.viewmodels.BaseViewModel;
import com.jakewharton.rxbinding2.support.design.widget.RxNavigationView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class NavigationDrawerActivity<T extends BaseViewModel> extends BaseLoggedInActivity<T>
{
    private ActionBarDrawerToggle drawerToggle;

    @Inject
    SparseArray<Class<? extends Activity>> menuMapping;

    protected @LayoutRes
    int getContentViewWrapper()
    {
        return R.layout.navigation_drawer_wrapper;
    }

    private @IdRes int getCurrentNavigationDrawerItem()
    {
        int result = menuMapping.indexOfValue(getClass());

        if(result >= 0)
        {
            result = menuMapping.keyAt(result);
        }

        return result;
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(layoutResID);

        initDrawer();
    }

    private void navigateTo(Class<?> activityClass)
    {
        startActivity(new Intent(this, activityClass)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    private void initDrawer()
    {
        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawerLayout);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolBar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
        toolBar.setNavigationOnClickListener(view -> drawerLayout.openDrawer(GravityCompat.START));

        NavigationView navigationView = (NavigationView)findViewById(R.id.navigationView);

        navigationView.setCheckedItem(getCurrentNavigationDrawerItem());

        RxNavigationView.itemSelections(navigationView)
                .observeOn(AndroidSchedulers.mainThread())
                .map(MenuItem::getItemId)
                .filter(id -> id != getCurrentNavigationDrawerItem())
                .map(menuMapping::get)
                .filter(activityClass -> activityClass != null)
                .compose(bindToLifecycle())
                .subscribe(this::navigateTo);
    }
}

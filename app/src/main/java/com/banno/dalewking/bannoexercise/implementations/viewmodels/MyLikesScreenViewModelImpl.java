package com.banno.dalewking.bannoexercise.implementations.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.viewmodels.MyLikesScreenViewModel;

import javax.inject.Inject;

import io.reactivex.Single;

public class MyLikesScreenViewModelImpl
        extends MediaListScreenViewModelImpl implements MyLikesScreenViewModel
{
    @Inject
    MyLikesScreenViewModelImpl()
    {
    }

    @Override
    protected Single<InstagramAccount.MediaListPage> getDataSource()
    {
        return instagramAccount.getMyLikes();
    }

}

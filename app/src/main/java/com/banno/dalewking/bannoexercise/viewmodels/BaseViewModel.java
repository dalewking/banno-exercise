package com.banno.dalewking.bannoexercise.viewmodels;

import com.banno.dalewking.bannoexercise.models.InstagramAccount;

public interface BaseViewModel
{
    boolean isUserLoggedIn();
}

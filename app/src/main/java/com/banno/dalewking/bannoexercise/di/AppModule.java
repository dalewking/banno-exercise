package com.banno.dalewking.bannoexercise.di;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.SparseArray;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.implementations.models.InstagramAccountImpl;
import com.banno.dalewking.bannoexercise.models.InstagramAccount;
import com.banno.dalewking.bannoexercise.views.MyLikesActivity;
import com.banno.dalewking.bannoexercise.views.MyMediaActivity;
import com.banno.dalewking.bannoexercise.views.MyUserActivity;
import com.f2prateek.rx.preferences2.RxSharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule
{
    private final Application application;

    AppModule(Application application)
    {
        this.application = application;
    }

    @Provides @Singleton
    Application provideApplication()
    {
        return application;
    }

    @Provides @Singleton
    InstagramAccount provideInstagramAccount(InstagramAccountImpl impl)
    {
        return impl;
    }

    @Provides @Singleton
    SharedPreferences providePreferences()
    {
        return application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
    }

    @Provides @Singleton
    RxSharedPreferences provideRxPreferences(SharedPreferences prefs)
    {
        return RxSharedPreferences.create(prefs);
    }

    @Provides @Singleton
    SparseArray<Class<? extends Activity>> provideNavigationMenuMapping()
    {
        SparseArray<Class<? extends Activity>> map = new SparseArray<>();

        map.put(R.id.myInfo, MyUserActivity.class);
        map.put(R.id.myMedia, MyMediaActivity.class);
        map.put(R.id.myLikes, MyLikesActivity.class);

        return map;
    }
}

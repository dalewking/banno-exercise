package com.banno.dalewking.bannoexercise.views;

import android.content.Intent;
import android.os.Bundle;

import com.banno.dalewking.bannoexercise.viewmodels.BaseViewModel;

import javax.inject.Inject;

public class BaseLoggedInActivity<T extends BaseViewModel> extends BaseActivity
{
    @Inject
    protected T viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        checkLoggedIn();
    }

    protected void checkLoggedIn()
    {
        if(!viewModel.isUserLoggedIn())
        {
            // TODO: Add a dialog pop-up telling them that are not logged in before going there
            startActivity(new Intent(this, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        }
    }
}

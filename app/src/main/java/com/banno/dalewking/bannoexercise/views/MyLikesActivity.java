package com.banno.dalewking.bannoexercise.views;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.viewmodels.MyLikesScreenViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class MyLikesActivity extends NavigationDrawerActivity<MyLikesScreenViewModel>
{
    @BindView(R.id.mediaList)
    RecyclerView recyclerView;

    private MediaListAdapter adapter;

    @Inject
    void setViewModel(MyLikesScreenViewModel viewModel)
    {
        this.viewModel = viewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.media_list_view);

        new MyLikesActivity_ViewBinding(this);

        adapter = new MediaListAdapter(R.layout.media_list_view_item);

        DividerItemDecoration dividerItemDecoration
                = new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        viewModel.getItems()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindToLifecycle())
                .subscribe(items -> adapter.update(items));

        viewModel.getState()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(bindToLifecycle())
                .doOnNext(state -> checkLoggedIn())
                .subscribe(adapter::updateState);

        adapter.getLoadRequests()
                .compose(bindToLifecycle())
                .subscribe(request -> viewModel.loadMoreItems());
    }
}
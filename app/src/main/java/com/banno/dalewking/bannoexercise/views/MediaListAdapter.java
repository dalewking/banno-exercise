package com.banno.dalewking.bannoexercise.views;

import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.banno.dalewking.bannoexercise.R;
import com.banno.dalewking.bannoexercise.models.InstagramApi;
import com.banno.dalewking.bannoexercise.viewmodels.MyMediaScreenViewModel;
import com.jakewharton.rxrelay2.PublishRelay;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

class MediaListAdapter extends RecyclerView.Adapter<MediaListAdapter.ItemListViewHolder>
{
    private List<InstagramApi.MediaItem> items = new ArrayList<>();

    private MyMediaScreenViewModel.State state = MyMediaScreenViewModel.State.MORE_DATA_TO_LOAD;

    private final @LayoutRes int itemLayout;

    private final PublishRelay<Object> loadMoreRequests = PublishRelay.create();

    MediaListAdapter(@LayoutRes int itemLayout)
    {
        this.itemLayout = itemLayout;
    }

    Observable<Object> getLoadRequests()
    {
        return loadMoreRequests;
    }

    private void loadMore()
    {
        loadMoreRequests.accept("load");
    }

    private boolean stateIsShown(MyMediaScreenViewModel.State state, int count)
    {
        switch(state)
        {
            case ERROR:
            case LOADING:
            case MORE_DATA_TO_LOAD:
                return true;

            default:
            case AUTHENTICATION_ERROR:
                return false;

            case NO_MORE_DATA:
                return count > 0;
        }
    }

    @Override
    public void onBindViewHolder(ItemListViewHolder holder, int position)
    {
        int itemViewType = getItemViewType(position);

        if(itemViewType == itemLayout)
        {
            holder.bind(items.get(position));
        }
        else if(itemViewType == R.layout.media_list_loading_item)
        {
            if (state == MyMediaScreenViewModel.State.MORE_DATA_TO_LOAD)
            {
                loadMore();
            }
        }
    }

    void update(List<InstagramApi.MediaItem> newItems)
    {
        final MediaItemDiffCallback diffCallback = new MediaItemDiffCallback(items, newItems);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.items.clear();
        this.items.addAll(newItems);

        diffResult.dispatchUpdatesTo(this);
    }

    void updateState(MyMediaScreenViewModel.State newState)
    {
        MyMediaScreenViewModel.State oldState = state;
        this.state = newState;

        boolean oldStateShown = stateIsShown(oldState, items.size());

        if(stateIsShown(newState, items.size()))
        {
            if(oldStateShown)
            {
                notifyItemChanged(items.size());
            }
            else
            {
                notifyItemInserted(items.size());
            }
        }
        else if(oldStateShown)
        {
            notifyItemRemoved(items.size());
        }
    }

    @Override
    public int getItemCount()
    {
        return items.size() + (stateIsShown(state, items.size()) ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position)
    {
        if(position < items.size())
        {
            return itemLayout;
        }
        else
        {
            switch (state)
            {
                case LOADING:
                case MORE_DATA_TO_LOAD:
                    return R.layout.media_list_loading_item;

                case NO_MORE_DATA:
                    return R.layout.no_items_layout;

                default:
                    return R.layout.media_list_error_item;
            }
        }
    }

    @Override
    public ItemListViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(viewType, parent, false);

        if(viewType == itemLayout)
        {
            return new MediaItemViewHolder(view);
        }
        else
        {
            switch (viewType)
            {
                case R.layout.media_list_loading_item:
                case R.layout.no_items_layout:
                    return new ItemListViewHolder(view);

                default:
                case R.layout.media_list_error_item:
                    return new ErrorItemViewHolder(view);
            }
        }
    }

    static class ItemListViewHolder extends RecyclerView.ViewHolder
    {
        ItemListViewHolder(View itemView)
        {
            super(itemView);
        }

        void bind(InstagramApi.MediaItem item)
        {
        }
    }

    class ErrorItemViewHolder extends ItemListViewHolder
    {
        ErrorItemViewHolder(View itemView)
        {
            super(itemView);

            new MediaListAdapter$ErrorItemViewHolder_ViewBinding(this, itemView);
        }

        @OnClick(R.id.retryButton)
        void retry()
        {
            loadMore();
        }
    }

    static class MediaItemViewHolder extends ItemListViewHolder
    {
        @Nullable @BindView(R.id.user)
        TextView user;

        @BindView(R.id.caption)
        TextView caption;

        @BindView(R.id.thumbNail)
        ImageView thumbNail;

        MediaItemViewHolder(View itemView)
        {
            super(itemView);

            new MediaListAdapter$MediaItemViewHolder_ViewBinding(this, itemView);
        }

        void bind(InstagramApi.MediaItem item)
        {
            if(user != null)
            {
                user.setText(item.from.fullName);
            }

            caption.setText(item.caption.text);

            Picasso.with(thumbNail.getContext())
                    .load(Uri.parse(item.images.thumbNail.url))
                    .into(thumbNail);
        }
    }
}

package com.banno.dalewking.bannoexercise.di;

import com.banno.dalewking.bannoexercise.views.ImageMediaActivity;
import com.banno.dalewking.bannoexercise.views.LoginActivity;
import com.banno.dalewking.bannoexercise.views.MyLikesActivity;
import com.banno.dalewking.bannoexercise.views.MyMediaActivity;
import com.banno.dalewking.bannoexercise.views.MyUserActivity;
import com.banno.dalewking.bannoexercise.views.SignInActivity;

import dagger.MembersInjector;

public interface AppInjectors
{
    MembersInjector<MyUserActivity> getMainActivityInjector();
    MembersInjector<MyMediaActivity> getMyMediaActivityInjector();
    MembersInjector<MyLikesActivity> getMyLikesActivityInjector();
    MembersInjector<ImageMediaActivity> getImageMediaActivityInjector();

    MembersInjector<LoginActivity> getLoginActivityInjector();

    MembersInjector<SignInActivity> getSignInActivityInjector();
}
